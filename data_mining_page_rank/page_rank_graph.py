import plotly.express as px
from page_rank import get_dict_with_page_ranks


dict_with_pagerank = get_dict_with_page_ranks()
print(dict_with_pagerank)

sorted_dict = {k: v for k, v in sorted(dict_with_pagerank.items(), key=lambda item: item[0])}
print(sorted_dict)

print(sorted(dict_with_pagerank.items(), key=lambda item: item[1]))


sites = list(sorted_dict.keys())
page_ranks = list(sorted_dict.values())
fig = px.line(x=sites, y=page_ranks, color=px.Constant("This year"),
             labels=dict(x="Sites", y="Amount", color="Time Period"))
fig.add_bar(x=sites, y=page_ranks, name="Last year")
fig.show()