import json
from copy import deepcopy


def get_dict_with_page_ranks():
    with open("data_mining_page_rank/result.txt", 'r') as file:
        dict_with_urls = json.loads(file.read())

    print(dict_with_urls)

    length = len(dict_with_urls)

    dict_with_page_ranks = {key: 1/length for key in dict_with_urls.keys()}

    for i in range(10):
        previous_page_ranks_dict = deepcopy(dict_with_page_ranks)
        for website in dict_with_urls.keys():
            list_with_inner_websites = []
            for key, value in dict_with_urls.items():
                if key == website:
                    continue

                if website in value:
                    list_with_inner_websites.append(key)

            page_rank = 0
            print(list_with_inner_websites, website)
            for inner_website in list_with_inner_websites:
                page_rank += previous_page_ranks_dict[inner_website] / len(dict_with_urls[inner_website])

            dict_with_page_ranks[website] = page_rank

    return dict_with_page_ranks


def main():
    result = get_dict_with_page_ranks()
    print(result)


if __name__ == '__main__':
    main()
