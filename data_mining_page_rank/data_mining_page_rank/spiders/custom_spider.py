import json

import scrapy
from bs4 import BeautifulSoup
import urllib.parse
import re
import logging

from scrapy.crawler import CrawlerProcess

logging.basicConfig(filename='example.log', level=logging.WARNING, filemode='w')
logging.getLogger('scrapy').propagate = False


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class CustomSpider(scrapy.Spider):
    name = "custom_spider"

    global_dict_with_links = {}

    def start_requests(self):
        urls = [
            'https://burgerkingrus.ru/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, cb_kwargs=dict(depth=0, url_from=url))

    def parse(self, response, depth=None, url_from=None):

        logging.info(response.url)

        domain_with_protocol = "{0.scheme}://{0.netloc}/".format(urllib.parse.urlsplit(response.url))
        only_netloc = urllib.parse.urlparse(domain_with_protocol).netloc

        only_netloc_for_url_from = urllib.parse.urlparse(url_from).netloc

        pattern_for_url = r"https?:\/\/"
        soup = BeautifulSoup(response.body)
        set_with_urls = set()

        list_with_links_in_website = soup.find_all('a')

        for link in list_with_links_in_website:
            url = str(link.get('href'))
            if not re.match(string=url, pattern=pattern_for_url):
                url = urllib.parse.urljoin(domain_with_protocol, url)

            link_domain = urllib.parse.urlparse(url).netloc

            if link_domain:
                set_with_urls.add(link_domain)
            else:
                set_with_urls.add(only_netloc)

            if not depth == 3:
                try:
                    yield scrapy.Request(url, callback=self.parse, cb_kwargs=dict(depth=depth + 1, url_from=response.url))
                except Exception as error:
                    print(f"{error} on url")

        if only_netloc_for_url_from not in self.global_dict_with_links.keys():
            self.global_dict_with_links[only_netloc_for_url_from] = set_with_urls
        else:
            self.global_dict_with_links[only_netloc_for_url_from] = self.global_dict_with_links[only_netloc_for_url_from] | set_with_urls

        logging.info(json.dumps(self.global_dict_with_links, cls=SetEncoder))


if __name__ == "__main__":
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    process.crawl(CustomSpider)
    process.start()
