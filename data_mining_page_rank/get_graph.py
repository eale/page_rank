import json

import networkx as nx
import matplotlib.pyplot as plt


def get_edges_for_graph(dict_for_analyze):
    result = []
    for key, value in dict_for_analyze.items():
        for website in value:
            if website == key:
                continue

            tuple_with_edge = (key, website)
            print(tuple_with_edge)

            if tuple_with_edge not in result:
                result.append(tuple_with_edge)

    return result


def main():
    with open("result.txt", "r") as file:
        dict_for_analyze = json.loads(file.read())

    edges = get_edges_for_graph(dict_for_analyze)

    print(edges)

    options = {
        'node_color': 'blue',
        'node_size': 40,
        'width': 1,
        'arrowstyle': '-|>',
        'arrowsize': 3,
    }

    G = nx.DiGraph()
    G.add_edges_from(edges)

    nx.draw_networkx(G, arrows=True, **options, with_labels=False)

    plt.show()


if __name__ == '__main__':
    main()